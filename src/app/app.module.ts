// Modulos
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';

// Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { WorkflowComponent } from './components/workflow/workflow.component';

import { NewWorkflowDialogComponent } from './components/shared/new-workflow-dialog/new-workflow-dialog.component';
import { NewTaskDialogComponent } from './components/shared/new-task-dialog/new-task-dialog.component';
import { TaskViewComponent } from './components/task-view/task-view.component';
import { WorkFlowEditDialogComponent } from './components/shared/work-flow-edit-dialog/work-flow-edit-dialog.component';
import { EditTaskDialogComponent } from './components/shared/edit-task-dialog/edit-task-dialog.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    WorkflowComponent,
    NewWorkflowDialogComponent,
    NewTaskDialogComponent,
    TaskViewComponent,
    WorkFlowEditDialogComponent,
    EditTaskDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
