import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WorkFlowModel } from '../models/workflow.model';
import { map, tap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { TaskModel } from '../models/task.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WorkflowsService {

  //private endpoint = 'https://localhost:44336/api/Workflow';
  private endpoint =  environment.apiUrl + '/Workflow';

  private _refresh$ = new Subject<void>();

  constructor( private http : HttpClient) { }

  get refresh$(){
    return this._refresh$;
  }


  // REST WORKFLOW 

  PostWorkflow( workflow: WorkFlowModel): Observable<any>{
    return this.http.post(this.endpoint, workflow)
      .pipe(
        tap(() => {
          this._refresh$.next();
        })
      ) 
  } 

  GetWorkflow( ) : Observable<any>{
    return this.http.get<WorkFlowModel[]>(this.endpoint);
  }

  DeleteWorkflow( id: string ): Observable<any>{
    
    return this.http.delete(`${this.endpoint}/${id}`)
      .pipe(
        tap(() => {
          this._refresh$.next();
        })
      ) 
  }

  UpdateWorkflow(workflow: WorkFlowModel): Observable<any>{
    return this.http.put<WorkFlowModel>(`${this.endpoint}`, workflow)
      .pipe(
        tap(() => {
          this._refresh$.next();
        })
      ) 
  }




  
  // REST TASK

  GetAllTasksFromWorkflow(id: string) : Observable<any>{    
    return this.http.get<TaskModel[]>(`${this.endpoint}/${id}/tasks`);
  }

  PostTask(id: string, Task: TaskModel ) :  Observable<any>{
    return this.http.post<TaskModel>(`${this.endpoint}/${id}/tasks`, Task)
      .pipe(
        tap(() => {
          this._refresh$.next();
        })
      ) 
  }

  DeleteTask(id: string) : Observable<any>{
    return this.http.delete<TaskModel>(`${this.endpoint}/${id}/task`)
    .pipe(
      tap(() => {
        this._refresh$.next();
      })
    ) 
  }

  PutTask( task: TaskModel){
    return this.http.put<TaskModel>(`${this.endpoint}/task`, task)
      .pipe(
        tap(()=>{
          this._refresh$.next();
        })
      )
  }
  
  DeleteAllTask(id:string) :Observable<any>{

    return this.http.delete<TaskModel>(`${this.endpoint}/${id}/taskAll`)
      .pipe(
        tap(() => {
          this._refresh$.next();
        })
      ) 
  }



}
