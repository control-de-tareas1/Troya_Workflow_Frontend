import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';


import { AuthGuard } from './guards/auth.guard';
import { TaskViewComponent } from './components/task-view/task-view.component';

const APP_ROUTES: Routes = [
  { path: "login",   component: LoginComponent},
  { path: "home",    component: HomeComponent, canActivate: [AuthGuard]},
  { path: "home/:Id", component: HomeComponent, canActivate: [AuthGuard]},
  { path: "**",      pathMatch:"full", redirectTo: "home"}
]



@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
