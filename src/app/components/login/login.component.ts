import { Component, OnInit } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Router } from '@angular/router'
import {AuthService} from '../../services/auth.service';

import { UsuarioModel } from '../../models/usuario.model';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel = new UsuarioModel();
  recordarme:boolean = false;



  constructor(
    overlayContainer: OverlayContainer,
    private auth: AuthService,
    private router: Router) {
    overlayContainer.getContainerElement().classList.add('light-theme');
  }
  

  ngOnInit(): void {

    if ( localStorage.getItem('email') ) {
      this.usuario.email = localStorage.getItem('email');
      this.recordarme = true;
    }
  }

  otherTheme: boolean = false;

  changeTheme(){
    this.otherTheme = !this.otherTheme;
  }
  
  login( form: NgForm ) {
    
    if (  form.invalid ) { return; }

    Swal.fire({  allowOutsideClick: false, icon: 'info', text: 'Espera por Favor..'});
    Swal.showLoading();


    this.auth.login(this.usuario)
      .subscribe( resp =>{

      Swal.close();
      
      if( this.recordarme){

        localStorage.setItem('email', this.usuario.email);
      }

      this.router.navigateByUrl('/home');

    },(erro)=>{
      
      Swal.fire({   icon: 'error', text: erro.error.title, title:'Error al Autenticar'});
 
    });
  }

   
}
