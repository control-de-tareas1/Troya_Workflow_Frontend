import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';


import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from "@angular/material/snack-bar";

import { WorkflowsService } from 'src/app/services/workflows.service';
import { WorkFlowModel } from '../../../models/workflow.model';
import { TaskModel } from '../../../models/task.model';



@Component({
  selector: 'app-edit-task-dialog',
  templateUrl: './edit-task-dialog.component.html',
  styleUrls: ['./edit-task-dialog.component.css']
})
export class EditTaskDialogComponent implements OnInit {

EditTask: TaskModel
  constructor(
    private snackBar: MatSnackBar,
    private workflowService: WorkflowsService,
    public dialogRef: MatDialogRef<EditTaskDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(this.data.Task);
    this.EditTask = this.data.Task;
    console.log(this.EditTask);
  }


  ngOnInit(): void {
  }

  editTask() {
    this.workflowService.PutTask(this.EditTask).
    subscribe((EditTask:TaskModel)=>{
      this.dialogRef.close();
      this.snackBar.open(`Se ha actualizado la tarea`, "cerrar", {duration: 1000}); 
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
