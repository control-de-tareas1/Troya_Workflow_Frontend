import { Component, Inject, OnInit  } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSnackBar } from "@angular/material/snack-bar";

import { WorkflowsService } from 'src/app/services/workflows.service';
import { TaskModel } from 'src/app/models/task.model';

@Component({
  selector: 'app-new-task-dialog',
  templateUrl: './new-task-dialog.component.html',
  styleUrls: ['./new-task-dialog.component.css']
})
export class NewTaskDialogComponent implements OnInit{

  workflowId: string;
  Task:  TaskModel = new TaskModel();

  constructor(
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private workflowService: WorkflowsService,
    public dialogRef: MatDialogRef<NewTaskDialogComponent> ,
           @Inject(MAT_DIALOG_DATA) public data: any){
             console.log( data.id);
            this.workflowId = this.data.id; 
            }
           
  ngOnInit() {

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  crearTask(){   
    this.workflowService.PostTask( this.workflowId, this.Task).subscribe((resp:TaskModel)=>{
      console.log(resp);
      this.dialogRef.close();
      this.snackBar.open(`Se ha agregado la tarea ${resp.nombre}`, "cerrar", {duration: 1000});
    })
  }


  
  



}
