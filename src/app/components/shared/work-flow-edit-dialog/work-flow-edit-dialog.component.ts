import { Component, OnInit,Inject } from '@angular/core';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSnackBar } from "@angular/material/snack-bar";

import { WorkflowsService } from '../../../services/workflows.service';
import { NewWorkflowDialogComponent } from '../new-workflow-dialog/new-workflow-dialog.component';
import { WorkFlowModel } from '../../../models/workflow.model';

@Component({
  selector: 'app-work-flow-edit-dialog',
  templateUrl: './work-flow-edit-dialog.component.html',
  styleUrls: ['./work-flow-edit-dialog.component.css']
})
export class WorkFlowEditDialogComponent implements OnInit {

  workFlowModel:WorkFlowModel;
  
  constructor(
    private snackBar: MatSnackBar,
    private workflowService: WorkflowsService,
    public dialogRef: MatDialogRef<WorkFlowEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any){ 
      this.workFlowModel = this.data.workFlow;
    }

  ngOnInit(): void {
  }
  

  updateWorkFlows() {
    this.workflowService.UpdateWorkflow(this.workFlowModel)
      .subscribe((flujo: WorkFlowModel) => {       
        this.dialogRef.close();
        this.snackBar.open(`Se ha actualizado el workflow`, "cerrar", {duration: 1000}); 
      })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
