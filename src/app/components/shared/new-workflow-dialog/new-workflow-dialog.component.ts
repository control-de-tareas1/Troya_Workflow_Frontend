import { Component, Inject } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSnackBar } from "@angular/material/snack-bar";

import { WorkflowsService } from 'src/app/services/workflows.service';
import { WorkFlowModel } from '../../../models/workflow.model';


@Component({
  selector: 'app-new-workflow-dialog',
  templateUrl: './new-workflow-dialog.component.html',
  styleUrls: ['./new-workflow-dialog.component.css']
})
export class NewWorkflowDialogComponent {

  workflow: WorkFlowModel = new WorkFlowModel();

  constructor(
    private snackBar: MatSnackBar,
    private router: Router,
    private workflowService: WorkflowsService,
    public dialogRef: MatDialogRef<NewWorkflowDialogComponent> ,
           @Inject(MAT_DIALOG_DATA) public message: string){ }
  
  onNoClick(): void {
    this.dialogRef.close();
  }
  
  crearWorkflow(){
    this.workflowService.PostWorkflow(this.workflow).subscribe((flujo:WorkFlowModel)=>{     
      this.dialogRef.close(); 
      this.snackBar.open(`Se ha agregado el workflow ${flujo.nombre}`, "cerrar", {duration: 1000});
      this.router.navigate([ 'home', flujo.id]); 
    }); 

  }

}
