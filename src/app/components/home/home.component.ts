import { Component, Input, OnInit, ɵConsole } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from "@angular/material/snack-bar";


import { AuthService  } from "../../services/auth.service";
import { WorkflowsService  } from "../../services/workflows.service";


import { WorkFlowModel  } from "../../models/workflow.model";
import { TaskModel  } from "../../models/task.model";

import { NewTaskDialogComponent } from '../shared/new-task-dialog/new-task-dialog.component';
import { EditTaskDialogComponent } from '../shared/edit-task-dialog/edit-task-dialog.component';

import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  tasks:  TaskModel[];
  selectedWorkflowId: string;
  suscription: Subscription;


  
  constructor(private snackBar: MatSnackBar,
              public dialog: MatDialog,
              private auth: AuthService,
              private router: Router,
              private workflowService: WorkflowsService,
              private route: ActivatedRoute){}

  ngOnInit(): void {

    this.getAlltasks();

    this.suscription = this.workflowService.refresh$.subscribe(()=>{
      this.getAlltasks();
    });
  }

  getAlltasks(){
    this.route.params.subscribe(
      (params: Params) => {
        if (params.Id) { 
          this.selectedWorkflowId = params.Id;
          this.workflowService.GetAllTasksFromWorkflow(params.Id).subscribe((tasks: TaskModel[]) => {
            this.tasks = tasks;
          })
        } else {
          this.tasks = undefined;
        }
      }
    )

  }

  salir(){

    this.auth.logout();
    this.router.navigateByUrl("/login");
  }

  openDialog(): void{
    const dialogRef = this.dialog.open( NewTaskDialogComponent, {
      width: '350px', data: { id: this.selectedWorkflowId}
    });
    dialogRef.afterClosed().subscribe( res => {
     
    })
  }

  deleteTask(id: string){
    this.workflowService.DeleteTask(id).subscribe(()=>{     
        
      this.snackBar.open(`Se ha eliminado la tarea`, "cerrar", {duration: 1000}); 

    })
  }
  openEditDialogTask(TaskModel:TaskModel):void {
    const dialogRef = this.dialog.open(EditTaskDialogComponent, {
      width: '350px', data:{Task:TaskModel}
    });
    dialogRef.afterClosed().subscribe(res => {

    })
  }
 
  deleteTaskAll(){
    this.workflowService.DeleteAllTask(this.selectedWorkflowId).subscribe(()=>{
      this.snackBar.open(`Se han eliminado todas las tareas`, "cerrar", {duration: 1000}); 
    })
  }
  
}
 