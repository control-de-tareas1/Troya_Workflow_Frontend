import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { NewWorkflowDialogComponent } from '../shared/new-workflow-dialog/new-workflow-dialog.component'
import { WorkFlowEditDialogComponent } from '../shared/work-flow-edit-dialog/work-flow-edit-dialog.component';

import { WorkFlowModel } from '../../models/workflow.model';

import { WorkflowsService } from '../../services/workflows.service';

import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from "@angular/material/snack-bar";

import { Subscription } from 'rxjs';



@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss']
})
export class WorkflowComponent implements OnInit {

  workflow: WorkFlowModel[];
  suscription: Subscription;



  constructor(
    private snackBar: MatSnackBar,
    private workflowService: WorkflowsService,
    public dialog: MatDialog,
    private router: Router, private route: ActivatedRoute) { }



  ngOnInit(): void {

    this.getWorkFlows();

    this.suscription = this.workflowService.refresh$.subscribe(() => {
      this.getWorkFlows();
    });
  }

  deleteWorkflow(flujo: WorkFlowModel, i: number): void {

    this.workflowService.DeleteWorkflow(flujo.id).subscribe(() => {
      this.snackBar.open(`Se ha eliminado el workflow`, "cerrar", {duration: 1000}); 
    });

  }


  getWorkFlows(): void {
    this.workflowService.GetWorkflow().subscribe((lists: any) => {
      this.workflow = lists;
    })

  }

  openEditDialog(workFlowModel:WorkFlowModel):void {
    const dialogRef = this.dialog.open(WorkFlowEditDialogComponent, {
      width: '350px', data:{workFlow:workFlowModel}
    });
    dialogRef.afterClosed().subscribe(res => {

    })
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NewWorkflowDialogComponent, {
      width: '350px'
    });
    dialogRef.afterClosed().subscribe(res => {

    })
  }


}